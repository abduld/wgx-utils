package utils

import "path/filepath"

func ParentDirectory(path string) string {
	dir := path
	if IsFile(path) {
		dir = filepath.Dir(dir)
	}
	return filepath.Dir(dir)
}
