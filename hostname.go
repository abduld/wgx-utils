package utils

import "os"

func GetHostname() (string, error) {
	name, err := os.Hostname()
	if err != nil {
		ip, err := GetIPAddress()
		if err != nil {
			return "", err
		}
		return "host-" + ip, nil
	}
	return name, nil
}
