package utils

// Memory represents a memory data type
type Memory int64

// Byte
const B Memory = 1

// KiloByte
const KB Memory = B * 1024

// MegaByte
const MB Memory = KB * 1024

// GigaByte
const GB Memory = MB * 1024
