package utils

import (
	"net"

	"gitlab.com/abduld/wgx-errors"
)

var (
	privateBlocks []*net.IPNet
)

func isPrivateIP(ipAddr string) bool {
	ip := net.ParseIP(ipAddr)
	for _, priv := range privateBlocks {
		if priv.Contains(ip) {
			return true
		}
	}
	return false
}

func GetIPAddress() (string, error) {

	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return "", errors.New("Failed to get interface addresses!").Wrap(err)
	}
	var ipAddr []byte
	for _, raw := range addrs {
		var ip net.IP
		switch addr := raw.(type) {
		case *net.IPAddr:
			ip = addr.IP
		case *net.IPNet:
			ip = addr.IP
		default:
			continue
		}

		if ip.To4() == nil {
			continue
		}

		if !isPrivateIP(ip.String()) {
			continue
		}

		ipAddr = ip
		break
	}

	if ipAddr == nil {
		return "", errors.New("No private IP address found, and explicit IP not provided")
	}

	return net.IP(ipAddr).String(), nil
}

func init() {
	for _, b := range []string{"10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16"} {
		if _, block, err := net.ParseCIDR(b); err == nil {
			privateBlocks = append(privateBlocks, block)
		}
	}
}
